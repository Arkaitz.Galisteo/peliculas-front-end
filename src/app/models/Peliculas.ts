import { Title } from '@angular/platform-browser';

export interface Peliculas{
    Title: string;
    Year: string;
    Type: string;
    Poster: string;
    imdbID: string
    Assessment: number;
}