import { Component, ViewChild, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PeliculasService } from "./services/peliculas.service";
import { Peliculas } from "./models/Peliculas";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private peliculasService: PeliculasService) { }

  film = 'harry potter';
  apikey = '4a249f8d'; //731e41f
  //apikey = '731e41f'; //Probar con otra key
  films: Peliculas[] = [];
  filmsFilter: Peliculas[] = [];


  ngOnInit() {
    this.getPeliculas();    
  }

  getPeliculas() {

    fetch(`http://www.omdbapi.com/?s=${this.film}&apikey=${this.apikey}`).then(
      res => {
        res.json().then(
          res => {
            res.Search.forEach((film: any) => {
              let movie: Peliculas = {
                Title: film['Title'],
                Year: film['Year'],
                Type: film['Type'],
                Poster: film['Poster'],
                imdbID: film['imdbID'],
                Assessment: 0
              }
              this.films.push(movie)
            });
            this.filmsFilter = this.films;            
            this.sendFilmsToDatabase();
          }
        )
      }
    )
  }

  sendFilmsToDatabase(){
    this.films.forEach(movie => {
      this.peliculasService.getPelicula(movie.imdbID).subscribe(
        res =>{
          if (res.length == 0) {            
            this.peliculasService.createPelicula(movie).subscribe()
          }else{
            if (res[0]['Assessment'] != 0) {
              console.log(res[0]['Assessment']);
              for (let i = 0; i < this.films.length; i++) {
                const film = this.films[i];
                if (film['imdbID'] == movie.imdbID) {
                  this.films[i]['Assessment'] = res[0]['Assessment']
                  this.filmsFilter[i]['Assessment'] = res[0]['Assessment']
                }         
              }
            }
            
          }
        },
        err => console.log('There is no database conection.')
      )
    });   
  }

  applyFilter(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.filmsFilter = this.filterMovies(filterValue);
    filterValue.toLocaleLowerCase();
  }

  filterMovies(filterValue: any) {
    return this.films.filter((film: { [x: string]: any; }) => {
      return film['Title'].toLocaleLowerCase().includes(filterValue.toLocaleLowerCase());
    });
  }

  updateFilm(id: any){    
    this.films.forEach(movie => {
      if (movie['imdbID'] == id) {
        console.log('Update ' + id);
        this.peliculasService.updatePelicula(id, movie).subscribe(
          res=>console.log(res)
        )
      }
    });
  }
}
