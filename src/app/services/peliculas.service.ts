import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Peliculas } from "../models/Peliculas";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  API_URI = 'http://localhost:3000/peliculas';

  constructor(private http:HttpClient) { }


  getPeliculas(): Observable<any>{
    return this.http.get<any>(`${this.API_URI}`);
  }

  getPelicula(Id:string): Observable<any>{
    return this.http.get<any>(`${this.API_URI}/${Id}`);
  }

  createPelicula(pelicula: Peliculas){
    return this.http.post(`${this.API_URI}`, pelicula);
  }

  updatePelicula(Id:string, pelicula: Peliculas){
    return this.http.put(`${this.API_URI}/${Id}`, pelicula);
  }
}
