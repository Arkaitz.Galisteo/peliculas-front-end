# Peliculas

Hola, voy a poner aquí las preguntas de la prueba. Un saludo

## Si te asignan una tarea y ves que la funcionalidad no está definida al 100%, ¿qué haces?

En el caso de que una funcionalidad no esté al 100% definida, que es algo que suele ocurrir, lo que hago es preguntar primero a mis compañeros/superiores y al cliente. En el caso de que el cliente no me la pueda definir mejor, intentaría hablarlo con mis superiores o compañeros para buscar una solución de cómo llevar a cabo la tarea.

## ¿Has utilizado algún sistema de control de versiones? ¿Cómo lo usabas? 

Estoy familiarizado con git. Lo suelo utilizar para dos cosas principalmente, la primera para poder compartir el código con los compañeros fácilmente y la segunda para mediante los commit tener una especie de guardado de seguridad. En la rama master subo una versión estable de lo que esté trabajando y en la rama de desarrollo mis pruebas y versiones no estables. En el caso de que tenga una versión que no funciones y sea mejor tirar atrás, es muy útil volver a commits anteriores.
